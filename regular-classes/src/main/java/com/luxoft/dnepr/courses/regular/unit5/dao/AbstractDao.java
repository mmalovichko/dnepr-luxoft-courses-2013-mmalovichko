package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Map;

public abstract class AbstractDao<T extends Entity> implements IDao<T> {
    Map<Long, Entity> entities = EntityStorage.getEntities();


    public AbstractDao() {

    }


    @Override
    public T save(T t) throws UserAlreadyExist {

        if (t.getId() == null) {
            t.setId(new Long(getNextId() + 1));
            //getNextId();
        } else if (entities.containsKey(t.getId())) {
            throw new UserAlreadyExist("", t.getId());
        }
        entities.put(t.getId(), t);
        return t;

    }

    public Long getNextId(){
        long id = 0;
        for (Long key:entities.keySet()){
            if (key > id){
                id = key;
            }
        }
        return id;


    }

    @Override
    public T update(T t) throws UserNotFound {
        Long idNewEntity = t.getId();
        if (!entities.containsKey(idNewEntity) || idNewEntity == null) {
            throw new UserNotFound("", idNewEntity);
        } else {
            entities.remove(idNewEntity);
            entities.put(idNewEntity, t);
            return t;
        }
    }

    @Override
    public T get(long id) {
        if (!entities.containsKey(id)) {
            return null;
        }
        return (T) entities.get(id);

    }

    @Override
    public boolean delete(long id) {
        if (entities.containsKey(id)) {
            entities.remove(id);
            return true;
        } else return false;
    }
}
