package com.luxoft.dnepr.courses.regular.unit18.model;


import java.util.HashSet;
import java.util.Set;

public class KnownWords {
    private Set<String> knownWords;

    public KnownWords(){
        knownWords = new HashSet<>();
    }

    public Set<String> getKnownWords() {
        return knownWords;
    }

}
