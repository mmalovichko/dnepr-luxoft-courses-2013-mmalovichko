package com.luxoft.dnepr.courses.regular.unit18.view;


public interface Command {
    String HELP = "help";
    String YES = "y";
    String NO = "n";
    String EXIT = "exit";
    String GET_MY_VOCABULARY = "get";

}
