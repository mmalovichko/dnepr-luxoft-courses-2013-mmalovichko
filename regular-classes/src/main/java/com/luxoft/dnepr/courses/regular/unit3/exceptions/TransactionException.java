package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class TransactionException extends Exception {

    public String getMessage() {
        return message;
    }

    private String message;

    public TransactionException(String message) {
        this.message = message;

    }

}
