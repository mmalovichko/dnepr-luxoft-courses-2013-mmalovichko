package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        this.id = id;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
    }

    public Wallet() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(id, "Wallet " + id + " is blocked");
        } else if (amountToWithdraw.compareTo(amount) == 1) {
            throw new InsufficientWalletAmountException(getId(), amountToWithdraw, getAmount(), "InsufficientFunds");
        }

    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        setAmount(amount.subtract(amountToWithdraw));
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(id, "Account is blocked");
        } else if (amountToTransfer.add(amount).compareTo(maxAmount) == 1) {
            throw new LimitExceededException(id, amountToTransfer, amount, "Limit Wallet Exceeded");
        }
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        setAmount(amount.add(amountToTransfer));
    }
}
