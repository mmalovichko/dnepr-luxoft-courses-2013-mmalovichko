package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class WalletIsBlockedException extends Exception {

    public Long getWalletId() {
        return walletId;
    }

    public String getMessage() {
        return message;
    }

    private Long walletId;
    private String message;

    public WalletIsBlockedException(Long walletId, String message){
        this.walletId = walletId;
        this.message = message;

    }

}
