package com.luxoft.dnepr.courses.regular.unit18.hinduSourse;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.StringTokenizer;

public class Vocabulary {
    public HashSet<String> voc = new HashSet<String>();

    public Vocabulary() {




        Reader r = new InputStreamReader(getClass().getResourceAsStream("/sonnets.txt"));
        System.out.println("after loading");
        String s = new String();
        try {

            for (int i; (i = r.read()) > 0; ) s = s + ((char) i);
            //System.out.println(s);
            r.close();
            StringTokenizer t = new StringTokenizer(s.toString());
            while (t.hasMoreTokens()) {
                String to = t.nextToken();
                if (to.length() > 3) voc.add(to.toLowerCase());
            }
        } catch
                (IOException e) {
        }
    }

    public String random() {
        String s = voc.toArray(new String[]{})[(int) (Math.random() * voc.size())];
        return s;

    }
}
