package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct implements Product {
    private boolean alcogol;

    public Beverage(String code, String name, double price, boolean alcogol){
        super(code, name, price);
        this.alcogol = alcogol;
    }

    public void setNonAlcogolic(boolean alcogol){
        this.alcogol = alcogol;

    }

    public boolean isNonAlcogolic(){
        return alcogol;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        if (alcogol != beverage.alcogol) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (alcogol ? 1 : 0);
        return result;
    }


}
