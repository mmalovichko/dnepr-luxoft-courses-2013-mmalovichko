package com.luxoft.dnepr.courses.regular.unit2;


public abstract class AbstractProduct implements Product, Cloneable {
    private String code;
    private String name;
    private double price;

    public AbstractProduct (String code, String name, double price){
        this.code = code;
        this.name = name;
        this.price = price;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractProduct that = (AbstractProduct) o;

        if (!code.equals(that.code)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }


    public String getName() {
        return this.name;
    }


    public double getPrice() {
        return this.price;
    }


}
