package com.luxoft.dnepr.courses.regular.unit18.model;


import java.util.HashSet;
import java.util.Set;

public class AllWords {
    private Set<String> vocabulary;

    public AllWords() {
        vocabulary =  new HashSet<>();
    }

    public Set<String> getVocabulary() {
        return vocabulary;
    }

    public void setVocabulary(Set<String> vocabulary) {
        this.vocabulary = vocabulary;
    }
}
