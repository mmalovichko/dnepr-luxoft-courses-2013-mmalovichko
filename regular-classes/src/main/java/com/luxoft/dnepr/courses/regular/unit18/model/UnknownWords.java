package com.luxoft.dnepr.courses.regular.unit18.model;


import java.util.HashSet;
import java.util.Set;

public class UnknownWords {
    private Set<String> unknownWords;

    public UnknownWords (){
        unknownWords = new HashSet<>();
    }

    public Set<String> getUnknownWords() {
        return unknownWords;
    }

}
