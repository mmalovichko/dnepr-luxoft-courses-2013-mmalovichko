package com.luxoft.dnepr.courses.regular.unit3;


public class User implements UserInterface {
    private long id;
    private String name;
    private WalletInterface wallet;

    public User(long id, String name, WalletInterface wallet) {
        this.wallet = wallet;
        this.id = id;
        this.name = name;
        wallet = new Wallet();
    }

    public User() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public WalletInterface getWallet() {
        return wallet;
    }

    @Override
    public void setWallet(WalletInterface wallet) {
        this.wallet = wallet;
    }
}
