package com.luxoft.dnepr.courses.regular.unit18.hinduSourse;

import java.util.Scanner;


public class UI implements Runnable {
    public void run() {
        String w, st;
        Vocabulary vocab = new Vocabulary();
        for (; ; ) {


            w = vocab.random();

            Words.res(vocab.voc.size());
            System.out.println("know: "+Words.knwn);
            System.out.println("nknown: "+Words.nknown);
            System.out.println("Size of vocab: "+vocab.voc.size()+ " Do you know translation of this word?:");
            System.out.println(w);


// get a boolean here, null for errors
            Boolean a = null;
            Scanner s = new Scanner(System.in);
            st = s.next();
            if (st != null && (st.isEmpty() || st.equals("exit"))) {
                // do result
                Words.res(vocab.voc.size());
                return;
            }
            if (st != null && (st.isEmpty() || st.equals("help"))) {//help info
                System.out.println("Lingustic analizator v1, autor Tushar Brahmacobalol for help type help, answer y/n question, your english knowlege will give you");
                continue;
            }
            if (st != null) a = st.equalsIgnoreCase("Y");
            if (a != null) {
            } else {
                System.out.println("Error while reading answer");
                return;
            }
            Words.ans(w, a);
        }
    }

    public static void main(String[] args) {
        UI ui = new UI();
        ui.run();
    }
}
