package com.luxoft.dnepr.courses.regular.unit18.hinduSourse;

public class Words {
    public static java.util.Vector<String> knwn = new java.util.Vector();
    public static java.util.Vector<String> nknown = new java.util.Vector();

    public static void res(int tot) {
        int w = tot * (knwn.size() + 1) / (knwn.size() + nknown.size() + 1);
        System.out.println("Your estimated vocabulary is " + w + " words");
    }

    public static void ans(String w, Boolean a) {
        knwn.add(a ? w : "");
        nknown.add(!a ? w : "");
    }

}
