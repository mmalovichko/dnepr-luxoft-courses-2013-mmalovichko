package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;

public abstract class AbstractDao<T extends Entity> implements IDao<T> {
    //Map<Long, Entity> entities = EntityStorage.getEntities();


    public AbstractDao() {

    }


    @Override
    public synchronized T save(T t) throws EntityAlreadyExistException {
        //synchronized(this){
        if (t.getId() == null) {
            t.setId(new Long(EntityStorage.getNextId()));}else
//        } else if (EntityStorage.getEntities().containsKey(t.getId())) {
//            throw new EntityAlreadyExistException();
//        }
            EntityStorage.getEntities().put(t.getId(), t);
        return t;
        // }

    }


    @Override
    public T update(T t) throws EntityNotFoundException {
        Long idNewEntity = t.getId();
        if (!EntityStorage.getEntities().containsKey(idNewEntity) || idNewEntity == null) {
            throw new EntityNotFoundException();
        } else {
            EntityStorage.getEntities().remove(idNewEntity);
            EntityStorage.getEntities().put(idNewEntity, t);
            return t;
        }
    }

    @Override
    public T get(long id) {
        if (!EntityStorage.getEntities().containsKey(id)) {
            return null;
        }
        return (T) EntityStorage.getEntities().get(id);

    }

    @Override
    public boolean delete(long id) {
        if (EntityStorage.getEntities().containsKey(id)) {
            EntityStorage.getEntities().remove(id);
            return true;
        } else return false;
    }
}