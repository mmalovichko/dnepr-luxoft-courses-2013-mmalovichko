package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Bank implements BankInterface {
    private Map<Long, UserInterface> users;
    private String actualJavaVersion;


    public Bank(String expectedJavaVersion) {
        String pattern = createPattern(expectedJavaVersion);
        actualJavaVersion = System.getProperty("java.version");
        if (!actualJavaVersion.matches(pattern)) {
            throw new IllegalJavaVersionError(actualJavaVersion, expectedJavaVersion, "Wrong Java Version Error");
        }
        users = new HashMap<Long, UserInterface>();
    }

    public Bank() {
        users = new HashMap<Long, UserInterface>();

    }

    public String createPattern(String expectedJavaVersion) {
        StringBuilder sb = new StringBuilder(expectedJavaVersion.substring(0, 3));
        sb.append(".*");
        return sb.toString();
    }


    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;

    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount)
            throws NoUserFoundException, TransactionException {

        if (userIsFind(fromUserId) && userIsFind(toUserId)) {
            UserInterface user1 = users.get(fromUserId);
            UserInterface user2 = users.get(toUserId);
            try {
                user1.getWallet().checkWithdrawal(amount);
            } catch (WalletIsBlockedException wibe) {
                throw new TransactionException("User '" + getUserNameFromWalletId(wibe.getWalletId()) + "' wallet is blocked");
            } catch (InsufficientWalletAmountException iwae) {
                throw new TransactionException("User '" + getUserNameFromWalletId(iwae.getWalletId()) + "' has insufficient funds" +
                        " (" + iwae.getAmountInWallet() + " < " + iwae.getAmountToWithdraw() + ")");
            }
            try {
                user2.getWallet().checkTransfer(amount);
            } catch (WalletIsBlockedException wibe) {
                throw new TransactionException("User '" + getUserNameFromWalletId(wibe.getWalletId()) + "' wallet is blocked");
            } catch (LimitExceededException lee) {
                throw new TransactionException("User '" + getUserNameFromWalletId(lee.getWalletId()) + "' wallet limit exceeded (" +
                        lee.getAmountInWallet() + " + " + lee.getAmountToTransfer() + " > " + user2.getWallet().getMaxAmount() + ")");
            }
            user1.getWallet().withdraw(amount);
            user2.getWallet().transfer(amount);
        }

    }

    public String getUserNameFromWalletId(Long walleId) {
        for (UserInterface u : users.values()) {
            if (u.getWallet().getId().equals(walleId)) {
                return u.getName();
            }
        }
        return null;

    }

    private boolean userIsFind(Long userId) throws NoUserFoundException, TransactionException {
        if (!users.containsKey(userId)) {
            throw new NoUserFoundException(userId, "User " + userId + " not found");
        } else return true;
    }


}
