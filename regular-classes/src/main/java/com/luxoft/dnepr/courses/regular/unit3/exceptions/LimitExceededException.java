package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class LimitExceededException extends Exception {

    private Long walletId;
    private BigDecimal amountToTransfer;
    private BigDecimal amountInWallet;
    private String message;

    public String getMessage() {
        return message;
    }

    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getAmountToTransfer() {
        return amountToTransfer.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getAmountInWallet() {
        return amountInWallet.setScale(2, BigDecimal.ROUND_HALF_UP);
    }


    public LimitExceededException(Long walletId, BigDecimal amountToTransfer, BigDecimal amountInWallet, String message) {
        this.walletId = walletId;
        this.amountToTransfer = amountToTransfer;
        this.amountInWallet = amountInWallet;
        this.message = message;

    }

}
