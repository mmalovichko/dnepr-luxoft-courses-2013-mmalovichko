package com.luxoft.dnepr.courses.regular.unit5.exception;


public class UserAlreadyExist extends Exception {
    private String message;
    private Long id;

    public UserAlreadyExist(String message, Long id){
        this.message=message;
        this.id=id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
