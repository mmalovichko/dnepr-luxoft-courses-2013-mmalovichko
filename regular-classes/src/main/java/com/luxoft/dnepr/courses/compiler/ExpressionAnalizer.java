package com.luxoft.dnepr.courses.compiler;


import java.io.ByteArrayOutputStream;

public class ExpressionAnalizer {

    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String DIVIDE = "/";
    public static final String MULTIP = "*";
    public static int COUNTER = 0;


    public static void elementalAnaliz(String input, ByteArrayOutputStream result) {
        Double[] array;
        if (input.contains(PLUS)) {
            array = splitting(input, "\\+");
            Compiler.addCommand(result, VirtualMachine.PUSH, array[0]);
            Compiler.addCommand(result, VirtualMachine.PUSH, array[1]);
            Compiler.addCommand(result, VirtualMachine.ADD);
            Compiler.addCommand(result, VirtualMachine.PRINT);
        } else if (input.contains(MINUS)) {
            array = splitting(input, "\\-");
            Compiler.addCommand(result, VirtualMachine.PUSH, array[0]);
            Compiler.addCommand(result, VirtualMachine.PUSH, array[1]);
            Compiler.addCommand(result, VirtualMachine.SWAP);
            Compiler.addCommand(result, VirtualMachine.SUB);
            Compiler.addCommand(result, VirtualMachine.PRINT);
        } else if (input.contains(DIVIDE)) {
            array = splitting(input, "\\/");
            Compiler.addCommand(result, VirtualMachine.PUSH, array[0]);
            Compiler.addCommand(result, VirtualMachine.PUSH, array[1]);
            Compiler.addCommand(result, VirtualMachine.SWAP);
            Compiler.addCommand(result, VirtualMachine.DIV);
            Compiler.addCommand(result, VirtualMachine.PRINT);
        } else if (input.contains(MULTIP)) {
            array = splitting(input, "\\*");
            Compiler.addCommand(result, VirtualMachine.PUSH, array[0]);
            Compiler.addCommand(result, VirtualMachine.PUSH, array[1]);
            Compiler.addCommand(result, VirtualMachine.MUL);
            Compiler.addCommand(result, VirtualMachine.PRINT);
        }
    }

    public static Double[] splitting(String input, String sign) {
        String[] s = input.split(sign);
        Double[] array = new Double[2];
        array[0] = Double.parseDouble(s[0]);
        array[1] = Double.parseDouble(s[1]);
        return array;

    }

    public static String clearSpaces(String input) {
        StringBuffer sb = new StringBuffer(input);
        while (sb.indexOf(" ") > 0) {
            int i = sb.indexOf(" ");
            sb.deleteCharAt(i);
        }
        return sb.toString();

    }

    public static void hardAnaliz(String input, ByteArrayOutputStream result) {
        //TODO: breakers and long expressions
    }

    public static void countSigns(String input) {
        COUNTER = 0;
        char[] c = input.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == '\u002B' || c[i] == '\u002D' || c[i] == '\u002F' || c[i] == '\u002A') {
                COUNTER++;
            }
        }
    }


}
