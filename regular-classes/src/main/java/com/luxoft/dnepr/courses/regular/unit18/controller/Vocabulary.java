package com.luxoft.dnepr.courses.regular.unit18.controller;


import com.luxoft.dnepr.courses.regular.unit18.model.AllWords;
import com.luxoft.dnepr.courses.regular.unit18.model.KnownWords;
import com.luxoft.dnepr.courses.regular.unit18.model.UnknownWords;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;

public class Vocabulary {
    private static final String SOURSE_PATH = "/sonnets.txt";
    private Set<String> vocabulary;
    private String localVocabulary[];

    public Set<String> getKnown() {
        return known;
    }

    public Set<String> getUnknown() {
        return unknown;
    }

    private Set<String> known;
    private Set<String> unknown;


    public Vocabulary() {
        vocabulary = new AllWords().getVocabulary();
        known = new KnownWords().getKnownWords();
        unknown = new UnknownWords().getUnknownWords();
    }

    public void initVocabulary() throws IOException {
        String line;

        BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(SOURSE_PATH)));
        while ((line = reader.readLine()) != null) {
            StringTokenizer t = new StringTokenizer(line);
            while (t.hasMoreTokens()) {
                String word = t.nextToken();
                if (word.length() > 3) {
                    vocabulary.add(word);

                }

            }

        }

        localVocabulary = vocabulary.toArray(new String[]{});
    }


    public String getNextRandomWord() {
        int randomNumber = new Random().nextInt(vocabulary.size() + 1);
        return localVocabulary[randomNumber];

    }

    public void addKnowWord(String word) {
        known.add(word);

    }

    public void addUnKnownWord(String word) {
        unknown.add(word);

    }

}
