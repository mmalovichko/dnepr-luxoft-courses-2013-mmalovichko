package com.luxoft.dnepr.courses.regular.unit4;


import java.util.*;

public class EqualSet<E> implements Set<E> {

    List eset = null;
    List l = new ArrayList();


    public EqualSet() {
        eset = new ArrayList<E>();
    }

    public EqualSet(Collection<? extends E> collection) {
        eset = new ArrayList<E>();
        if (collection != null) {
            for (Iterator iter = collection.iterator(); iter.hasNext(); ) {
                Object obj = iter.next();
                if (!eset.contains(obj)) {
                    eset.add(obj);
                }

            }
        }
    }

    @Override
    public int size() {
        return eset.size();
    }

    @Override
    public boolean isEmpty() {
        return eset.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return eset.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return eset.iterator();
    }

    @Override
    public Object[] toArray() {
        return eset.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return (T[]) eset.toArray(a);
    }

    @Override
    public boolean add(E e) {
        boolean added = false;
        if (!eset.contains(e)) {
            eset.add(e);
            added = true;
        }
        return added;
    }

    @Override
    public boolean remove(Object o) {
        return eset.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return eset.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        int counter = 0;
        for (E fd:c){
                if (!eset.contains(fd)) {
                eset.add(fd);
                counter++;
            }
        }
        return counter > 0;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return eset.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return eset.removeAll(c);
    }

    @Override
    public void clear() {
        eset.clear();
    }

}
