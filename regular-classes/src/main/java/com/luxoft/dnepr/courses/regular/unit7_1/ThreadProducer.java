package com.luxoft.dnepr.courses.regular.unit7_1;


public final class ThreadProducer {
    private static volatile Thread myThread;

    private ThreadProducer() {
    }


    public static Thread getNewThread() {
        return new Thread() {
            public void run() {
            }
        };

    }

    public static Thread getRunnableThread() {
        myThread = new Thread() {
            public void run() {
                for (; ; ) {
                }
            }
        };
        myThread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return myThread;
    }

    public static Thread getBlockedThread() {
        new Thread() {
            public void run() {
                forOneThread();
            }
        }.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        myThread = new Thread() {
            public void run() {
                forOneThread();
            }
        };
        myThread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return myThread;

    }

    public static Thread getTimedWaitingThread() {
        myThread = new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000000000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };
        myThread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return myThread;

    }

    public static synchronized void forOneThread() {
        while (true) {
        }
    }

    public static Thread getWaitingThread() {
        myThread = new Thread() {
            public void run() {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        myThread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return myThread;
    }

    public static Thread getTerminatedThread() {
        myThread = new Thread() {
            public void run() {
            }
        };
        myThread.start();
        try {
            //Thread.sleep(1000);
            myThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return myThread;
    }


}
