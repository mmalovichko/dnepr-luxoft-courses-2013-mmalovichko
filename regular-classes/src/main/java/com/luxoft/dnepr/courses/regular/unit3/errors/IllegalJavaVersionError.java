package com.luxoft.dnepr.courses.regular.unit3.errors;

public class IllegalJavaVersionError extends Error {

    private String actualJavaVersion;
    private String expectedJavaVersion;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getActualJavaVersion() {
        return actualJavaVersion;
    }

    public void setActualJavaVersion(String actualJavaVersion) {
        this.actualJavaVersion = actualJavaVersion;
    }

    public String getExpectedJavaVersion() {
        return expectedJavaVersion;
    }

    public void setExpectedJavaVersion(String expectedJavaVersion) {
        this.expectedJavaVersion = expectedJavaVersion;
    }

    public IllegalJavaVersionError(String actualJavaVersion, String expectedJavaVersion, String message){
        this.expectedJavaVersion = expectedJavaVersion;
        this.message = message;
        this.actualJavaVersion = actualJavaVersion;

    }

}
