package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill extends CompositeProduct implements Product {
    Map<Product, CompositeProduct> cp1 = new HashMap<Product, CompositeProduct>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        if (!cp1.containsKey(product)) {
            cp1.put(product, new CompositeProduct());
        }
        cp1.get(product).add(product);
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double totalSum = 0;
        Collection<CompositeProduct> cd = cp1.values();

        for (CompositeProduct c : cd) {
            totalSum += c.getPrice();
        }
        return totalSum;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        List<Product> list = new ArrayList<Product>();
        Collection<CompositeProduct> cd = cp1.values();
        for (CompositeProduct c : cd) {
            list.add(c);
        }
        Collections.sort(list, new InnerClassForCompare());

        return list;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

    class InnerClassForCompare implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            CompositeProduct p1 = (CompositeProduct) o1;
            CompositeProduct p2 = (CompositeProduct) o2;

            if (p1.getPrice() <= p2.getPrice()) {
                return 1;
            } else return -1;
        }
    }

}
