package com.luxoft.dnepr.courses.regular.unit18;


import com.luxoft.dnepr.courses.regular.unit18.view.CommandLineInterface;

import java.io.IOException;

public class Start {

    public static void main(String[] args) throws IOException {
        new CommandLineInterface().runAnalize();
    }
}
