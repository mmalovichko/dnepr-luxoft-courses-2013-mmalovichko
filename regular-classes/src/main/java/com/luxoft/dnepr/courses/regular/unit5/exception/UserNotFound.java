package com.luxoft.dnepr.courses.regular.unit5.exception;


public class UserNotFound extends Exception {
    private Long id;
    private String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserNotFound(String message, Long id) {

        this.id = id;
        this.message = message;
    }
}
