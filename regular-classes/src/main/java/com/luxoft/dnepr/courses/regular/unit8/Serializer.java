package com.luxoft.dnepr.courses.regular.unit8;

import java.io.*;

public class Serializer {
    public static void serialize(File file, FamilyTree entity) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
            entity.writeExternal(out);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    public static FamilyTree deserialize (File file) {
        FamilyTree famtree = new FamilyTree();

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){
            famtree.readExternal(ois);
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return famtree;

    }


}
