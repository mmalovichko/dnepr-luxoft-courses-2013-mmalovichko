package com.luxoft.dnepr.courses.regular.unit18.view;


import com.luxoft.dnepr.courses.regular.unit18.controller.Vocabulary;

import java.io.IOException;
import java.util.Scanner;

public class CommandLineInterface {
    private static boolean doingFlag;
    private Vocabulary vocabulary;


    public CommandLineInterface(){
        doingFlag = true;
        vocabulary = new Vocabulary();

    }

    public void runAnalize() {
        try {
            vocabulary.initVocabulary();
        } catch (IOException e) {
            System.out.println("Exception: "+e.getMessage());
            e.printStackTrace();
        }

        while (doingFlag) {

            System.out.print("Do you know translation of this word?: ");
            String indicatedWord = vocabulary.getNextRandomWord();
            System.out.println(indicatedWord);

            Scanner s = new Scanner(System.in);
            String command = s.next();
            execute(command, indicatedWord);

        }

    }


    private void execute(String command,String indicatedWord) {
         if (command.equalsIgnoreCase(Command.HELP)){
             printHelp();
         } else if (command.equalsIgnoreCase(Command.YES)){
             vocabulary.addKnowWord(indicatedWord);
         } else if (command.equalsIgnoreCase(Command.NO)){
             vocabulary.addUnKnownWord(indicatedWord);
         } else if (command.equalsIgnoreCase(Command.EXIT)){
             doingFlag = false;
         } else if (command.equalsIgnoreCase(Command.GET_MY_VOCABULARY)){
             printMyVocabulary();
         } else {
             System.out.println("Invalid command");
         }
    }

    private void printMyVocabulary() {
        System.out.println("---------------------------------");
        System.out.println("Your vocabulary is " + vocabulary.getKnown().size() + " words");
        System.out.println("You need study " + vocabulary.getUnknown().size() + " words");
        System.out.println("---------------------------------");
        System.out.println("Known words: " + vocabulary.getKnown().toString());
        System.out.println("---------------------------------");
        System.out.println("Unknown words: " + vocabulary.getUnknown().toString());
        System.out.println("---------------------------------");

    }


    private static void printHelp() {
        System.out.println("========================\n" +
                "Linguistic analizator, v 1.0\n" +
                "author Max@\n" +
                "Commands: \n" +
                "\"help\" - print this help\n" +
                "\"y\" / \"Y\" - yes, I know indicated word\n" +
                "\"n\" / \"N\" - no, I don't know indicated word\n" +
                "\"exit\" - out of the programm\n" +
                "\"get\" - to print statistics\n" +
                "========================\n");
    }

}
