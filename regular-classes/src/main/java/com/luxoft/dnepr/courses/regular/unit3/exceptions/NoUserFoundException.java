package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class NoUserFoundException extends Exception {

    private Long userId;
    private String message;

    public Long getUserId() {
        return userId;
    }


    public NoUserFoundException(Long userId, String message) {
        this.userId = userId;
        this.message = message;

    }

}
