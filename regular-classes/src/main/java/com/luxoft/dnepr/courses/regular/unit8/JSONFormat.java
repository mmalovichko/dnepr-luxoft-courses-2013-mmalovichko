package com.luxoft.dnepr.courses.regular.unit8;


import java.io.IOException;
import java.io.ObjectInput;
import java.lang.reflect.Field;
import java.util.Date;

public class JSONFormat {
    private static final String beginFile = "{\"root\":";
    private static final String endFileObject = "}";
    private static final String beginObject = "{";
    private static final String komma = ",";
    private static final String colon = ":";
    private static final String quote = "\"";
    private static int howMuchObjects = 0;
    private static StringBuffer sb = new StringBuffer(beginFile);
    private static int indexCounter;

    public static int getHowMuchObjects() {
        return howMuchObjects;
    }

    public JSONFormat() {
    }

    public static String getBeginFile() {

        return beginFile;
    }

    public static String getEndFileObject() {
        return endFileObject;
    }

    public static String getBeginObject() {
        return beginObject;
    }

    public static StringBuffer toJSON(Person person) {
        howMuchObjects++;
        if (person != null) {
            if (howMuchObjects == 2) {
                sb.append("\"father\":");
            } else if (howMuchObjects == 3) {
                sb.append("},\"mother\":");
            }
            sb.append(beginObject);
            Field[] fields = person.getClass().getDeclaredFields();
            int fieldCounter = 0;
            for (Field f : fields) {

                try {
                    f.setAccessible(true);
                    if (!f.getType().equals(Person.class)) {
                        sb.append(quote);
                        sb.append(f.getName());
                        sb.append(quote);
                        sb.append(colon);
                        sb.append(quote);
                        if (f.getType().equals(Date.class)){
                            Date date = (Date)(f.get(person));
                            if(date == null){
                                sb.append("null");
                            }
                            else sb.append(date.toGMTString());
                        } else {
                            sb.append(f.get(person));
                        }
                        sb.append(quote);
                        fieldCounter++;
                        if (!(fieldCounter == fields.length - 2) || (howMuchObjects == 1)) sb.append(komma);
                    } else {
                        toJSON((Person) f.get(person));
                    }
                } catch (IllegalAccessException ie) {
                    ie.printStackTrace();
                } finally {
                    f.setAccessible(false);
                }
            }
        } else {
            howMuchObjects--;

        }

        return sb;
    }


    public static Person fromJSON(ObjectInput in) throws IOException, IllegalAccessException, InstantiationException {
        howMuchObjects = 0;
        indexCounter = 0;
        FamilyTree famtree = new FamilyTree(new Person());
        StringBuffer sb = new StringBuffer(in.readLine());

        Person me = famtree.getRoot();
        me = setPersonFromJSON(me, sb);
        me.setFather(setPersonFromJSON(new Person(), sb));
        me.setMother(setPersonFromJSON(new Person(), sb));
        return me;

    }

    private static Person setPersonFromJSON(Person person, StringBuffer sb) {
        String gen;
        switch (howMuchObjects){
            case 0:{person.setName(getVarFromJSONName("root", sb, 11, 0));
                System.out.println(person.getName());}break;
            case 1:person.setName(getVarFromJSONName("father", sb, 11, 0));break;
            case 2:person.setName(getVarFromJSONName("mother", sb, 11, 0));break;
        }
        gen = getVarFromJSONName("gender", sb, 3, indexCounter);
        if (gen == null){
            person.setGender(null);
        } else person.setGender(Gender.valueOf(getVarFromJSONName("gender", sb, 3, indexCounter)));
        person.setEthnicity(getVarFromJSONName("ethnicity", sb, 3, indexCounter));
        person.setBirthDate(new Date(getVarFromJSONName("birthDate", sb, 3, indexCounter)));
        howMuchObjects++;
        return person;


    }

    private static String getVarFromJSONName(String var, StringBuffer sb, int step, int findFrom) {
        int endOfNameVar = sb.indexOf(var, findFrom)+var.length();
        int beginVar = endOfNameVar + step;
        int endVar = sb.indexOf("\"", beginVar);
        String variable = sb.substring(beginVar, endVar);
        if(variable.equals("null")){
            return null;
        }
        indexCounter = sb.indexOf(var, findFrom);
        return variable;
    }
}
