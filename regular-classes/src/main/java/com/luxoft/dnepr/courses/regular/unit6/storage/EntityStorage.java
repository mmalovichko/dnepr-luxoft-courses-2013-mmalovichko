package com.luxoft.dnepr.courses.regular.unit6.storage;


import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

import java.util.concurrent.ConcurrentHashMap;

public class EntityStorage {
    private static ConcurrentHashMap<Long, Entity> entities = new ConcurrentHashMap<>();

    private EntityStorage() {
    }

    public synchronized static ConcurrentHashMap<Long, Entity> getEntities() {
        return entities;
    }

    public static synchronized Long getNextId() {
        long id = 0;
        //counter++;
        for (Long key : entities.keySet()) {
            if (key > id) {
                id = key;
            }
        }
        entities.put(id+1, new Entity());
        return id+1;
//        if (counter > id + 1) {
//            entities.put(counter, NULL_VALUE);
//            return counter;
//        } else {
//            counter = id + 1;
//            entities.put(counter, NULL_VALUE);
//            return id + 1;
//        }
    }


}
