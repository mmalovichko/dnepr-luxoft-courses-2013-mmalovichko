package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class InsufficientWalletAmountException extends Exception {

    private Long walletId;
    private BigDecimal amountToWithdraw;
    private BigDecimal amountInWallet;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getAmountToWithdraw() {
        return amountToWithdraw.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getAmountInWallet() {
        return amountInWallet.setScale(2, BigDecimal.ROUND_HALF_UP);
    }


    public InsufficientWalletAmountException(Long walletId, BigDecimal amountToWithdraw, BigDecimal amountInWallet, String message) {
        this.walletId = walletId;
        this.amountToWithdraw = amountToWithdraw;
        this.amountInWallet = amountInWallet;
        this.message = message;
    }

}
