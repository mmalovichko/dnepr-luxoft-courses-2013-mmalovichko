package com.luxoft.dnepr.courses.regular.unit2;

import junit.framework.Assert;
import org.junit.Test;

import java.util.GregorianCalendar;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book.clone();
        Assert.assertEquals(true, book.equals(cloned));

    }

    @Test
    public void testEquals() throws Exception {
        Book book1 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book2 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book) book1.clone();

        Assert.assertEquals(book1.equals(book2), book2.equals(book1));
        Assert.assertEquals(cloned.equals(book2), book1.equals(cloned));

    }
}