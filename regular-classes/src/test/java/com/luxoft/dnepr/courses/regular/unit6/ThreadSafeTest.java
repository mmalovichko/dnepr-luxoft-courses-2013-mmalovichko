package com.luxoft.dnepr.courses.regular.unit6;


import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class ThreadSafeTest  {
    private volatile Thread myThread;
    private static Map<Long, Entity> entities = null;




    @Before
    public void initStorage() {
        entities = EntityStorage.getEntities();
    }



    @Test
    public void testGetNextId() throws InterruptedException {
        for (int i = 0; i < 30; i++){
            new Thread(){
                public void run(){
                    for (int i = 0; i < 1000; i++) {
                        EntityStorage.getNextId();
                    }

                }
            }.start();
        }
        Thread.sleep(1000);
//        for (Long key : TeachersStorage.getEntities().keySet()) {
//            System.out.println("value: "+key.toString());
//        }
        //Assert.assertEquals(30000, TeachersStorage.getEntities().size());
        Assert.assertEquals(30000, EntityStorage.getEntities().size());

    }



}
