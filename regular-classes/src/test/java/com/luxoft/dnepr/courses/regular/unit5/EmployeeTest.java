package com.luxoft.dnepr.courses.regular.unit5;


import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Map;

public class EmployeeTest {
    private Map<Long, Entity> entities;
    private IDao<Employee> edi;
    private IDao<Redis> redi;

    @Before
    public void createEntityStorage() {
        entities = EntityStorage.getEntities();
        edi = new EmployeeDaoImpl();
        redi = new RedisDaoImpl();
    }

    @After
    public void clearEntityStorage() {
        entities.clear();
    }

    @Test
    public void testEmployeeDaoImplSave1() throws UserAlreadyExist {
        edi.save(new Employee(1L, 3500));
        edi.save(new Employee(2L, 5500));
        edi.save(new Employee(3L, 6500));
        Assert.assertEquals(3, entities.size());

    }


    @Test
    public void testEmployeeDaoImplSave2() throws UserAlreadyExist {
        edi.save(new Employee(1L, 3500));
        edi.save(new Employee(2L, 5500));
        try {
            edi.save(new Employee(2L, 6500));
            Assert.fail("Same id, there must be fail");
        } catch (UserAlreadyExist uae) {
            Assert.assertEquals(new Long(2), uae.getId());
            Assert.assertEquals("", uae.getMessage());
        }
        edi.save(new Employee(3L, 5500));
        Assert.assertEquals(3, entities.size());

    }

    @Test
    public void testEmployeeDaoImplSave3() throws UserAlreadyExist {
        edi.save(new Employee(1L, 3500));
        edi.save(new Employee(2L, 5500));
        edi.save(new Employee(null, 5700));
        edi.save(new Employee(null, 7700));
        Assert.assertEquals(4, entities.size());
        Collection<Entity> list = entities.values();
        Long i = 1L;
        for (Entity e : list) {
            Assert.assertEquals(i, e.getId());
            i++;
        }

    }

    @Test
    public void testEmployeeDaoImplDelete() throws UserAlreadyExist {
        edi.save(new Employee(1L, 3500));
        edi.save(new Employee(2L, 4500));
        edi.save(new Employee(3L, 5700));
        edi.save(new Employee(4L, 10300));
        Assert.assertEquals(true, edi.delete(1L));
        Assert.assertEquals(false, edi.delete(5L));
        //edi.delete(1L);
        Assert.assertEquals(3, entities.size());
        Collection<Entity> list = entities.values();
        Long i = 2L;
        for (Entity e : list) {
            Assert.assertEquals(i, e.getId());
            i++;
        }

    }

    @Test
    public void testEmployeeDaoImplGet() throws UserAlreadyExist {
        Employee edi1 = new Employee(1L, 3500);
        Employee edi2 = new Employee(2L, 3500);
        edi.save(new Employee(1L, 3500));
        edi.save(new Employee(2L, 4500));
        edi.save(new Employee(3L, 5700));
        edi.save(new Employee(4L, 10300));
        Assert.assertEquals(true, edi1.equals(edi.get(1L)));
        Assert.assertEquals(false, edi2.equals(edi.get(2L)));

    }

    @Test
    public void testEmployeeDaoImplUpdate1() throws UserAlreadyExist, UserNotFound {
        edi.save(new Employee(1L, 3500));
        edi.save(new Employee(2L, 4500));
        edi.save(new Employee(3L, 5700));
        edi.save(new Employee(4L, 10300));
        Employee edi1 = edi.update(new Employee(1L, 100_000));
        Employee edi2 = edi.get(1L);
        Assert.assertEquals(100_000, edi1.getSalary());
        Assert.assertEquals(100_000, edi2.getSalary());

    }

    @Test
    public void testEmployeeDaoImplUpdate2() throws UserAlreadyExist, UserNotFound {
        edi.save(new Employee(1L, 3500));
        edi.save(new Employee(2L, 4500));
        edi.save(new Employee(3L, 5700));
        edi.save(new Employee(4L, 10300));
        Employee notInTheStorage1 = new Employee(5L, 2376);
        try {
            edi.update(notInTheStorage1);
            Assert.fail("User must be not found");
        } catch (UserNotFound unf) {
            Assert.assertEquals(new Long(5L), unf.getId());
            Assert.assertEquals("", unf.getMessage());
        }

        Employee notInTheStorage2 = new Employee(null, 2376);
        try {
            edi.update(notInTheStorage2);
            Assert.fail("User must be not found");
        } catch (UserNotFound unf) {
            Assert.assertEquals(null, unf.getId());
            Assert.assertEquals("", unf.getMessage());
        }


    }


}
