package com.luxoft.dnepr.courses.regular.unit5;


import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;

public class RedisTest {
    private Map<Long, Entity> entities;
    private IDao<Redis> redi;

    @Before
    public void createEntityStorage() {
        entities = EntityStorage.getEntities();
        redi = new RedisDaoImpl();
    }

    @After
    public void clearEntityStorage() {
        entities.clear();
    }

    @Test
    public void testRedisDaoImplSave1() throws UserAlreadyExist, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        redi.save(new Redis(1L, 3500));
        redi.save(new Redis(2L, 5500));
        redi.save(new Redis(3L, 6500));
        Assert.assertEquals(3, entities.size());

    }


    @Test
    public void testRedisDaoImplSave2() throws UserAlreadyExist, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        redi.save(new Redis(1L, 3500));
        redi.save(new Redis(2L, 5500));
        try {
            redi.save(new Redis(2L, 6500));
            Assert.fail("Same id, there must be fail");
        } catch (UserAlreadyExist uae) {
            Assert.assertEquals(new Long(2), uae.getId());
            Assert.assertEquals("", uae.getMessage());
        }
        redi.save(new Redis(3L, 5500));
        Assert.assertEquals(3, entities.size());

    }

    @Test
    public void testRedisDaoImplSave3() throws UserAlreadyExist, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        redi.save(new Redis(1L, 3500));
        redi.save(new Redis(2L, 5500));
        redi.save(new Redis(null, 5700));
        redi.save(new Redis(null, 7700));
        Assert.assertEquals(4, entities.size());
        Collection<Entity> list = entities.values();
        Long i = 1L;
        for (Entity e : list) {
            Assert.assertEquals(i, e.getId());
            i++;
        }

    }

    @Test
    public void testRedisDaoImplDelete() throws UserAlreadyExist, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        redi.save(new Redis(1L, 3500));
        redi.save(new Redis(2L, 4500));
        redi.save(new Redis(3L, 5700));
        redi.save(new Redis(4L, 10300));
        Assert.assertEquals(true, redi.delete(1L));
        Assert.assertEquals(false, redi.delete(5L));
        //edi.delete(1L);
        Assert.assertEquals(3, entities.size());
        Collection<Entity> list = entities.values();
        Long i = 2L;
        for (Entity e : list) {
            Assert.assertEquals(i, e.getId());
            i++;
        }

    }

    @Test
    public void testRedisDaoImplGet() throws UserAlreadyExist, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Redis edi1 = new Redis(1L, 3500);
        Redis edi2 = new Redis(2L, 3500);
        redi.save(new Redis(1L, 3500));
        redi.save(new Redis(2L, 4500));
        redi.save(new Redis(3L, 5700));
        redi.save(new Redis(4L, 10300));
        Assert.assertEquals(true, edi1.equals(redi.get(1L)));
        Assert.assertEquals(false, edi2.equals(redi.get(2L)));

    }

    @Test
    public void testRedisDaoImplUpdate1() throws UserAlreadyExist, UserNotFound, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        redi.save(new Redis(1L, 3500));
        redi.save(new Redis(2L, 4500));
        redi.save(new Redis(3L, 5700));
        redi.save(new Redis(4L, 10300));
        Redis edi1 = redi.update(new Redis(1L, 100_000));
        Redis edi2 = redi.get(1L);
        Assert.assertEquals(100_000, edi1.getWeight());
        Assert.assertEquals(100_000, edi2.getWeight());

    }

    @Test
    public void testRedisDaoImplUpdate2() throws UserAlreadyExist, UserNotFound, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        redi.save(new Redis(1L, 3500));
        redi.save(new Redis(2L, 4500));
        redi.save(new Redis(3L, 5700));
        redi.save(new Redis(4L, 10300));
        Redis notInTheStorage1 = new Redis(5L, 2376);
        try {
            redi.update(notInTheStorage1);
            Assert.fail("User must be not found");
        } catch (UserNotFound unf) {
            Assert.assertEquals(new Long(5L), unf.getId());
            Assert.assertEquals("", unf.getMessage());
        }

        Redis notInTheStorage2 = new Redis(null, 2376);
        try {
            redi.update(notInTheStorage2);
            Assert.fail("User must be not found");
        } catch (UserNotFound unf) {
            Assert.assertEquals(null, unf.getId());
            Assert.assertEquals("", unf.getMessage());
        }


    }


}
