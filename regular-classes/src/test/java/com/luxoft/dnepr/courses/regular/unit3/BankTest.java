package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BankTest {

    private List<WalletInterface> list = new ArrayList<WalletInterface>();
    private Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();
    Bank bank;

    @Before
    public void createWalletsAndUsers() {
        list.add(new Wallet(1L, new BigDecimal(1000), WalletStatus.ACTIVE, new BigDecimal(10000)));
        list.add(new Wallet(2L, new BigDecimal(107), WalletStatus.BLOCKED, new BigDecimal(1000)));
        list.add(new Wallet(3L, new BigDecimal(300), WalletStatus.BLOCKED, new BigDecimal(150)));
        list.add(new Wallet(4L, new BigDecimal(0), WalletStatus.BLOCKED, new BigDecimal(10000)));
        list.add(new Wallet(5L, new BigDecimal(5005), WalletStatus.ACTIVE, new BigDecimal(1000)));
        list.add(new Wallet(6L, new BigDecimal(100), WalletStatus.ACTIVE, new BigDecimal(1000)));
        list.add(new Wallet(7L, new BigDecimal(100), WalletStatus.ACTIVE, new BigDecimal(200)));
        bank = new Bank("1.7");
        users.put(1L, new User(1L, "Max", list.get(0)));
        users.put(2L, new User(2L, "Gregory", list.get(1)));
        users.put(3L, new User(3L, "Andy", list.get(2)));
        users.put(4L, new User(4L, "Mike", list.get(3)));
        users.put(5L, new User(5L, "Lesly", list.get(4)));
        users.put(6L, new User(6L, "Nick", list.get(5)));
        users.put(7L, new User(7L, "Mikey", list.get(6)));
        bank.setUsers(users);

    }

    @Test
    public void testMakeMoneyTransactionUserNotFound() throws TransactionException, NoUserFoundException {

        try {
            bank.makeMoneyTransaction(8L, 6L, new BigDecimal(10));
            Assert.fail("there must be exception!");
        } catch (NoUserFoundException nofe) {
            Assert.assertEquals(Long.valueOf(8), nofe.getUserId());
            Assert.assertEquals("User 8 not found", "User " + nofe.getUserId() + " not found");
        }

        try {
            bank.makeMoneyTransaction(5L, 90L, new BigDecimal(10));
            Assert.fail("there must be exception!");
        } catch (NoUserFoundException nofe) {
            Assert.assertEquals(Long.valueOf(90), nofe.getUserId());
            Assert.assertEquals("User 90 not found", "User " + nofe.getUserId() + " not found");
        }


        bank.makeMoneyTransaction(1L, 6L, new BigDecimal(10));
        Assert.assertEquals("990.00", list.get(0).getAmount().toString());
    }

    @Test
    public void testMakeMoneyTransactionUserIsBlocked() throws TransactionException, NoUserFoundException {
        try {
            bank.makeMoneyTransaction(2L, 6L, new BigDecimal(10));
            Assert.fail("There must be fail");
        } catch (TransactionException te) {
            Assert.assertEquals("User 'Gregory' wallet is blocked", te.getMessage());

        }

        try {
            bank.makeMoneyTransaction(1L, 3L, new BigDecimal(10));
            Assert.fail("There must be fail");
        } catch (TransactionException te) {
            Assert.assertEquals("User 'Andy' wallet is blocked", te.getMessage());

        }

    }

    @Test
    public void testMakeMoneyTransactionInsFunds() throws TransactionException, NoUserFoundException {
        try {
            bank.makeMoneyTransaction(1L, 6L, new BigDecimal(1001));
            Assert.fail("There must be fail");
        } catch (TransactionException te) {
            Assert.assertEquals("User 'Max' has insufficient funds (1000.00 < 1001.00)", te.getMessage());

        }

    }


    @Test
    public void testMakeMoneyTransactionWalletLimitExceed() throws TransactionException, NoUserFoundException {
        try {
            bank.makeMoneyTransaction(1L, 7L, new BigDecimal(500));
            Assert.fail("There must be fail");
        } catch (TransactionException te) {
            Assert.assertEquals("User 'Mikey' wallet limit exceeded (100.00 + 500.00 > 200.00)", te.getMessage());

        }

    }

    @Test
    public void testMakeMoneyTransactionSuccess1() throws TransactionException, NoUserFoundException {
        bank.makeMoneyTransaction(1L, 6L, new BigDecimal(50));
        Assert.assertEquals(new BigDecimal(950).setScale(2, BigDecimal.ROUND_HALF_UP), users.get(1L).getWallet().getAmount());
        Assert.assertEquals(new BigDecimal(150).setScale(2, BigDecimal.ROUND_HALF_UP), users.get(6L).getWallet().getAmount());

    }

    @Test
    public void testMakeMoneyTransactionSuccess2() throws TransactionException, NoUserFoundException {
        bank.makeMoneyTransaction(6L, 7L, new BigDecimal(0.99));
        Assert.assertEquals(new BigDecimal(99.01).setScale(2, BigDecimal.ROUND_HALF_UP), users.get(6L).getWallet().getAmount());
        Assert.assertEquals(new BigDecimal(100.99).setScale(2, BigDecimal.ROUND_HALF_UP), users.get(7L).getWallet().getAmount());

    }

    @Test
    public void testMakeMoneyTransactionSuccess3() throws TransactionException, NoUserFoundException {
        bank.makeMoneyTransaction(7L, 7L, new BigDecimal(0.99));
        Assert.assertEquals(new BigDecimal(100.00).setScale(2, BigDecimal.ROUND_HALF_UP), users.get(7L).getWallet().getAmount());

    }

    @Test
    public void testMakeMoneyTransactionSuccess4() throws TransactionException, NoUserFoundException {
        bank.makeMoneyTransaction(6L, 7L, new BigDecimal(-1));
        Assert.assertEquals(new BigDecimal(101.00).setScale(2, BigDecimal.ROUND_HALF_UP), users.get(6L).getWallet().getAmount());


    }


}
