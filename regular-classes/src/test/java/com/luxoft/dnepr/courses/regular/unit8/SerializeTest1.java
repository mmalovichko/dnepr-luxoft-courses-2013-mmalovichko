package com.luxoft.dnepr.courses.regular.unit8;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Date;


public class SerializeTest1 {
    FamilyTree ft;
    File file;

    @Test
    public void serealizeDeserializeTest() throws IOException {
        file = new File("D:\\luxoft\\testinggggg.json");

        Person father = new Person();
        father.setBirthDate(new Date("Sat, 19 MAY 1967 13:30:00 GMT"));
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Gena");

        Person mother = new Person();
        mother.setEthnicity("russian");
        mother.setName("Sveta");
        mother.setGender(Gender.FEMALE);
        mother.setBirthDate(new Date("Sun, 31 MARCH 1969 16:30:00 GMT"));

        Person me = new Person();
        me.setFather(father);
        me.setMother(mother);
        me.setName(null);
        me.setBirthDate(new Date("Mon, 17 APRIL 1990 20:30:00 GMT"));
        me.setGender(Gender.MALE);
        me.setEthnicity("ukr");
        ft = new FamilyTree(me);
        ft.getRoot().setEthnicity(null);
        Serializer.serialize(file, ft);
        FamilyTree result= Serializer.deserialize(file);
        Assert.assertEquals(true, ft.getRoot().getFather().equals(result.getRoot().getFather()));
        Assert.assertEquals(true, ft.getRoot().getMother().equals(result.getRoot().getMother()));
        Assert.assertEquals(true, ft.getRoot().equals(result.getRoot()));
    }
}
