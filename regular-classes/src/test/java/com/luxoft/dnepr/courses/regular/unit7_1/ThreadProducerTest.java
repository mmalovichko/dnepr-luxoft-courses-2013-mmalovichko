package com.luxoft.dnepr.courses.regular.unit7_1;


import org.junit.Assert;
import org.junit.Test;

public class ThreadProducerTest {

    @Test
    public void testGetNewThread() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(Thread.State.NEW, ThreadProducer.getNewThread().getState());
    }


    @Test
    public void testGetRunnableThread() {
        Assert.assertEquals(Thread.State.RUNNABLE, ThreadProducer.getRunnableThread().getState());
    }

    @Test
    public void testGetBlockedThread() throws InterruptedException {
        Assert.assertEquals(Thread.State.BLOCKED, ThreadProducer.getBlockedThread().getState());
    }

    @Test
    public void testGetTerminatedThread() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(Thread.State.TERMINATED, ThreadProducer.getTerminatedThread().getState());
    }

    @Test
    public void testGetTimedWaitingThread() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(Thread.State.TIMED_WAITING, ThreadProducer.getTimedWaitingThread().getState());
    }

    @Test
    public void testGetWaitingThread() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(Thread.State.WAITING,  ThreadProducer.getWaitingThread().getState());

    }
}
