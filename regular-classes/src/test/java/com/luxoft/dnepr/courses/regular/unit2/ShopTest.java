package com.luxoft.dnepr.courses.regular.unit2;


import junit.framework.Assert;
import org.junit.Test;

import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ShopTest {
    private ProductFactory factory = new ProductFactory();


    @Test
    public void testShop() throws Exception {
        Bill bill = new Bill();

        Bread bread = factory.createBread("bread1", "White fresh bread", 10, 1.5);
        bill.append(bread);
        Bread anotherBread = (Bread) bread.clone();
        anotherBread.setPrice(15);
        bill.append(anotherBread);
        Beverage cola = factory.createBeverage("beverage1", "Coca-cola", 5, true);
        bill.append(cola);
        bill.append((Product) cola.clone());
        bill.append((Product) cola.clone());
        Book javaBook = factory.createBook("book1", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        bill.append(javaBook);

        Assert.assertEquals("[Thinking in Java * 1 = 200.0, White fresh bread * 2 = 23.75, Coca-cola * 3 = 13.5]\n" +
                "Total cost: 237.25", bill.toString());


        List<Product> list = bill.getProducts();
        assertEquals((double) 200, list.get(0).getPrice(), 0.00001);
        assertEquals((double) 23.75, list.get(1).getPrice(), 0.00001);
        assertEquals((double) 13.5, list.get(2).getPrice(), 0.00001);

    }
}
