package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import junit.framework.Assert;
import org.junit.Test;

public class CreationBankTest {

    @Test
    public void testBankLowerJavaVersion() {
        try {
            Bank bank = new Bank("1.5");
            Assert.fail("there must be error!");
        } catch (IllegalJavaVersionError er) {
            Assert.assertEquals(System.getProperty("java.version"), er.getActualJavaVersion());
            Assert.assertEquals("Wrong Java Version Error", er.getMessage());
            Assert.assertEquals("1.5", er.getExpectedJavaVersion());
        }
    }

    @Test
    public void testBankUpperJavaVersion() {
        try {
            Bank bank = new Bank("1.8");
            Assert.fail("there must be error!");
        } catch (IllegalJavaVersionError er) {
            Assert.assertEquals(System.getProperty("java.version"), er.getActualJavaVersion());
            Assert.assertEquals("Wrong Java Version Error", er.getMessage());
            Assert.assertEquals("1.8", er.getExpectedJavaVersion());
            System.out.println(System.getProperty("java.version"));
        }

    }

    @Test
    public void testBankSame1JavaVersion() {
        Bank bank = new Bank("1.7");
    }

    @Test
    public void testBankSame2JavaVersion() {
        Bank bank = new Bank("1.7.3");
    }

    @Test
    public void testBankSame3JavaVersion() {
        Bank bank = new Bank("1.7.0_01");
    }


}
