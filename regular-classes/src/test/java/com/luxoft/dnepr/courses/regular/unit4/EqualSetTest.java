package com.luxoft.dnepr.courses.regular.unit4;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class EqualSetTest {
    private List list;
    private Set eset;

    @Before
    public void initCollection() {
        list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("4");
        list.add("5");
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);

    }


    @Test
    public void testDefaultConstr() {
        eset = new EqualSet();
        Assert.assertEquals(0, eset.size());
    }

    @Test
    public void testConstr() {
        eset = new EqualSet(list);
        Assert.assertEquals(6, eset.size());
    }

    @Test
    public void testIsEmpty() {
        eset = new EqualSet(list);
        Assert.assertEquals(false, eset.isEmpty());
        eset = new EqualSet();
        Assert.assertEquals(true, eset.isEmpty());
    }

    @Test
    public void testContains() {
        eset = new EqualSet();
        eset.add(null);
        Assert.assertEquals(true, eset.contains(null));
        eset.add("34");
        eset.add("34");
        Assert.assertEquals(true, eset.contains("34"));
    }

    @Test
    public void testToArray() {
        Object obj[] = new Object[]{"1", "2", "3", "4", "5", null};
        eset = new EqualSet(list);
        Object objTesting[] = eset.toArray();
        Assert.assertArrayEquals(obj, objTesting);
    }

    @Test
    public void testToArrayE() {
        String obj[] = new String[]{"1", "2", "3", "4", "5", null};
        eset = new EqualSet(list);
        Object objTesting[] = eset.toArray(obj);
        Assert.assertArrayEquals(obj, objTesting);
    }

    @Test
    public void testRemove() {
        eset = new EqualSet(list);
        eset.remove("1");
        Assert.assertEquals(5, eset.size());
        Assert.assertEquals(false, eset.contains("1"));
    }

    @Test
    public void testContainsAll() {
        eset = new EqualSet(list);
        Assert.assertEquals(true, eset.containsAll(list));

    }

    @Test
    public void testAddAll() {
        eset = new EqualSet();
        eset.addAll(list);
        Assert.assertEquals(6, eset.size());
    }

    @Test
    public void testRemoveAll() {
        eset = new EqualSet(list);
        eset.removeAll(eset);
        Assert.assertEquals(0, eset.size());
    }

    @Test
    public void testClear() {
        eset = new EqualSet(list);
        eset.clear();
        Assert.assertEquals(0, eset.size());
    }

    @Test
    public void testRetainAll1() {
        Set eset1 = new EqualSet(list);
        eset = new EqualSet();
        eset.add("1");
        eset1.retainAll(eset);
        Assert.assertEquals(1, eset1.size());
    }

    @Test
    public void testRetainAll2() {
        Set eset1 = new EqualSet(list);
        eset = new EqualSet();
        eset.add(null);
        eset1.retainAll(eset);
        Assert.assertEquals(1, eset1.size());
    }

    @Test
    public void testIterator() {
        eset = new EqualSet(list);
        Set eset1 = new EqualSet();
        int i = 0;
        Iterator itr = eset.iterator();
        while (itr.hasNext()) {
            eset1.add(itr.next());
            i++;
        }
        Assert.assertEquals(6, i);
        Assert.assertEquals(eset.size(), eset1.size());

    }


}
