package com.luxoft.dnepr.courses.regular.unit2;


import junit.framework.Assert;
import org.junit.Test;

public class BeverageTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Beverage bev = productFactory.createBeverage("Cola", "Coka-Cola", 12, false);
        Beverage bevCloned = (Beverage) bev.clone();
        Assert.assertEquals(true, bev.equals(bevCloned));

    }

    @Test
    public void testEquals() throws Exception {
        Beverage bev = productFactory.createBeverage("Cola", "Coka-Cola", 12, false);
        Beverage bev1 = productFactory.createBeverage("Cola", "Coka-Cola", 12, false);
        Beverage bev2 = (Beverage) bev.clone();

        Assert.assertEquals(bev.equals(bev1), bev1.equals(bev));
        Assert.assertEquals(bev.equals(bev2), bev2.equals(bev));

    }
}