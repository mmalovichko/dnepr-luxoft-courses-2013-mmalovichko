package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class CompositeProductTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testAdd() {
        CompositeProduct compositeProduct = new CompositeProduct();

        assertEquals(0, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(1, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(2, compositeProduct.getAmount());
    }

    @Test
    public void testGetPrice() {
        CompositeProduct compositeProduct = new CompositeProduct();

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(10, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals((10 + 10) * 0.95, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 20, true));
        assertEquals((10 + 10 + 20) * 0.9, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 520, true));
        assertEquals((10 + 10 + 20 + 520) * 0.9, compositeProduct.getPrice(), 0);
    }

    @Test
    public void testRemove() {
        CompositeProduct compositeProduct = new CompositeProduct();
        Beverage bev1 = factory.createBeverage("cola", "Coca-cola", 10, true);
        Beverage bev2 = factory.createBeverage("cola", "Coca-cola", 20, true);

        assertEquals(0, compositeProduct.getAmount());

        compositeProduct.add(bev1);
        compositeProduct.add(bev2);
        compositeProduct.remove(bev1);
        assertEquals(1, compositeProduct.getAmount());
        compositeProduct.remove(bev2);
        assertEquals(0, compositeProduct.getAmount());

    }

    @Test
    public void testGetName() {
        CompositeProduct compositeProduct = new CompositeProduct();

        assertEquals(null, compositeProduct.getName());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals("Coca-cola", compositeProduct.getName());

        compositeProduct.add(factory.createBeverage("coda", "sitr", 5, true));
        assertEquals("Coca-cola", compositeProduct.getName());
    }

    @Test
    public void testGetCode() {
        CompositeProduct compositeProduct = new CompositeProduct();

        assertEquals(null, compositeProduct.getCode());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals("cola", compositeProduct.getCode());

        compositeProduct.add(factory.createBeverage("coda", "sitr", 5, true));
        assertEquals("cola", compositeProduct.getCode());
    }
}