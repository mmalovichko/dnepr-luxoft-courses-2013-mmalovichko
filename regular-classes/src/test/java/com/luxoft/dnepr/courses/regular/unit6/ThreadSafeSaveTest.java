package com.luxoft.dnepr.courses.regular.unit6;


import com.luxoft.dnepr.courses.regular.unit6.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit6.model.Employee;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;




public class ThreadSafeSaveTest  {
    private IDao <Employee> emp;



    @Before
    public void initStorage() {
        emp = new EmployeeDaoImpl();
    }



    @Test
    public void testSave() throws InterruptedException {
        emp = new EmployeeDaoImpl();
        for (int i = 0; i < 30; i++){
            new Thread(){
                public void run(){
                    for (int i = 0; i < 1000; i++) {
                        emp.save(new Employee(EntityStorage.getNextId(), (int)Math.random()*100));
                    }

                }
            }.start();
        }
        Thread.sleep(1000);
        //Assert.assertEquals(30001, atl.longValue());
        Assert.assertEquals(30000, EntityStorage.getEntities().size());

    }

}
