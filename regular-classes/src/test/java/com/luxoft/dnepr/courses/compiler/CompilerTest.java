package com.luxoft.dnepr.courses.compiler;

import org.junit.Test;

public class CompilerTest extends AbstractCompilerTest {

    @Test
    public void testSimple() {
        assertCompiled(4, "2+2");
        assertCompiled(5, " 2 + 3 ");
        assertCompiled(1, "2 - 1 ");
        assertCompiled(6, " 2 * 3 ");
        assertCompiled(-2, " 5 -7  ");
        assertCompiled(0.6666666666666666, "  2/3 ");
        assertCompiled(0, "  0 -    0 ");
        assertCompiled(-1000, "  0   -   1000 ");
    }
	
//	@Test
//	public void testComplex() {
//		assertCompiled(12, "  (2 + 2 ) * 3 ");
//		assertCompiled(8.5, "  2.5 + 2 * 3 ");
//		assertCompiled(8.5, "  2 *3 + 2.5");
//	}
}
