package com.luxoft.courses.auth;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class FirstFilter1 implements Filter, UserParametres {
    private Map<String, LinkedList<String>> users = new ConcurrentHashMap<>();
    private FilterConfig config = null;
    private static int i = 0;

    public static Map<String, String> getSessIdLogonStorage() {
        return sessIdLogonStorage;
    }

    private static Map<String, String> sessIdLogonStorage = new ConcurrentHashMap<>();


    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        System.out.println(++i);
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        if (isLoginAndPassCorrect(request)) {
            createSession(request);
            chain.doFilter(req, resp);
            System.out.println(++i);
        } else if (!isLoginAndPassCorrect(request)) {
            sendResponseWrongPass(response);

        }


    }

    private void createSession(HttpServletRequest request) {
        request.getSession().setAttribute(request.getParameter("login"), users.get(request.getParameter("login")).get(GET_USER_ROLE));
        sessIdLogonStorage.put(request.getSession(false).getId(), request.getParameter("login"));
        System.out.println("session: "+sessIdLogonStorage.size());

    }


    private boolean isLoginAndPassCorrect(HttpServletRequest request) {
        System.out.println("checking login..."+request.getParameter("login"));
        return (users.containsKey(request.getParameter("login"))
                && users.get(request.getParameter("login")).get(GET_USER_PASSWORD).equals(request.getParameter("password")))
                ? true : false;
    }


    public void init(FilterConfig config) throws ServletException {
        this.config = config;

        String json = config.getServletContext().getInitParameter("users");
        Gson gson = new Gson();
        users = gson.fromJson(json, new TypeToken<Map<String, LinkedList<String>>>() {
        }.getType());


    }

    private void sendResponseWrongPass(HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.getWriter().print("<!DOCTYPE HTML>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/authoriz.css\">\n" +
                "    <title>Авторизация</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "<div id=\"authoriz\">\n" +
                "    <form id=\"data\" action=\"http://localhost:8080/user\" method=\"post\">\n" +
                "        <div id=\"login\">\n" +
                "            <input id=\"log\" type=\"text\" tabindex=\"1\" data-name=\"login\" name=\"login\" maxlength=\"15\" placeholder=\"Login\">\n" +
                "        </div>\n" +
                "        <div id=\"password\">\n" +
                "            <input id=\"log\" type=\"password\" tabindex=\"2\" form=\"data\" data-name=\"password\" name=\"password\"\n" +
                "                   placeholder=\"Password\">\n" +
                "        </div>\n" +
                "        <div id=\"log-in\">\n" +
                "            <button id=\"button\" tabindex=\"3\" type=\"submit\" src=\"images/backForm.jpg\">Log in</button>\n" +
                "            <a id=\"forgotpass\" href=\"index.html\">forgot your password?</a>\n" +
                "            <h5>Wrong login or password</h5>\n" +
                "        </div>\n" +
                "\n" +
                "    </form>\n" +
                "</div>\n" +
                "\n" +
                "</body>\n" +
                "</html>");
    }
}