package com.luxoft.courses.javaWeb;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StorageOfNames extends HttpServlet {
    private final static String JSON_BEGIN_ERROR = "{\"error\": ";
    private final static String JSON_END = "}";
    private final static String QUOTE = "\"";
    private static Map<String, Double> storageNames = new ConcurrentHashMap<>();
    StringBuffer reasonOfError = new StringBuffer();
    String name;
    String age;


    protected void doPut(HttpServletRequest request
            , HttpServletResponse response) throws IOException {

        parseParams(request);

        try {
            if (!isValidParams(name, age)) {
                reasonOfError.append("Illegal parameters");
                sendError(reasonOfError, response);
            } else if (storageNames.containsKey(name)) {

                reasonOfError.append("Name ").append(name).append(" already exists");
                sendError(reasonOfError, response);
            } else {

                storageNames.put(name, Double.valueOf(age));
                response.setStatus(HttpServletResponse.SC_CREATED);
            }
        } catch (NumberFormatException e) {
            reasonOfError.append("Illegal parameters");
            sendError(reasonOfError, response);
        } finally {
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/json");

        }

    }


    protected void doPost(HttpServletRequest request
            , HttpServletResponse response) throws IOException {

        parseParams(request);

        try {
            if (!isValidParams(name, age)) {
                reasonOfError.append("Illegal parameters");
                sendError(reasonOfError, response);
            } else if (!storageNames.containsKey(name)) {
                reasonOfError.append("Name " + name + " does not exist");
                sendError(reasonOfError, response);
            } else if (storageNames.containsKey(name)) {
                storageNames.put(name, Double.valueOf(age));
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
            }
        } catch (NumberFormatException e) {
            reasonOfError.append("Illegal parameters");
            sendError(reasonOfError, response);
        } finally {
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/json");
        }

    }

    private void parseParams(HttpServletRequest request) {
        this.name = request.getHeader("name");
        this.age = request.getHeader("age");
        reasonOfError.setLength(0);
    }

    private boolean isValidParams(String name, String age) {
        return (name == null || age == null || name.equals("") || age.equals("")) ? false : true;
    }


    private void sendError(StringBuffer reasonOfError, HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        PrintWriter writer = response.getWriter();
        writer.print(JSON_BEGIN_ERROR + QUOTE + reasonOfError.toString() + QUOTE + JSON_END);

    }


}
