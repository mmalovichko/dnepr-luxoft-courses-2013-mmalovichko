package com.luxoft.courses.javaWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;


public class Counter extends HttpServlet {
    private static AtomicInteger counter = new AtomicInteger(0) ;
    private final static String JSON_BEGIN = "{\"hitCount\": ";

    protected void doGet(HttpServletRequest request
            , HttpServletResponse response)
            throws ServletException, IOException {

        counter.addAndGet(1);
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        response.setCharacterEncoding("utf-8");
        PrintWriter writer = response.getWriter();
        writer.print(JSON_BEGIN+counter+"}");

    }
}
