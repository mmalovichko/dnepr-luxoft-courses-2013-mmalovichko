package com.luxoft.courses.authorization;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LoginServletLogic extends HttpServlet {
    private Map<String, String> users = new ConcurrentHashMap<>();
    private static Map<String, String> sessIdLogonStorage = new ConcurrentHashMap<>();

    public static Map<String, String> getSessIdLogonStorage() {
        return sessIdLogonStorage;
    }



    @Override
    public void init() throws ServletException {
        String json = getServletContext().getInitParameter("users");
        Gson gson = new Gson();
        users = gson.fromJson(json, new TypeToken<Map<String, String>>() {
        }.getType());

    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("http://localhost:8080/index.html");
    }

    protected void doPost(HttpServletRequest request
            , HttpServletResponse response) throws IOException {

        if (isLoginAndPassValid(request)) {
            createUserSession(request);
            response.sendRedirect("http://localhost:8080/user");
        } else {
            sendResponseWrongPass(response);
            //response.sendRedirect("http://localhost:8040/index.html");
        }

    }



    private void createUserSession(HttpServletRequest request) {
        request.getSession().setAttribute(request.getParameter("login"), request.getParameter("password").toString().hashCode());
        sessIdLogonStorage.put(request.getSession(false).getId(), request.getParameter("login"));

    }


    private void loginInside(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://localhost:8080/user");

    }


    private boolean isLoginAndPassValid(HttpServletRequest request) {
        return (users.containsKey(request.getParameter("login")) && users.get(request.getParameter("login")).equals(request.getParameter("password"))) ? true : false;

    }


    private void sendResponseWrongPass(HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.getWriter().print("<!DOCTYPE HTML>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/authoriz.css\">\n" +
                "    <title>Авторизация</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "<div id=\"authoriz\">\n" +
                "    <form id=\"data\" action=\"http://localhost:8080/login\" method=\"post\">\n" +
                "        <div id=\"login\">\n" +
                "            <input id=\"log\" type=\"text\" tabindex=\"1\" data-name=\"login\" name=\"login\" maxlength=\"15\" placeholder=\"Login\">\n" +
                "        </div>\n" +
                "        <div id=\"password\">\n" +
                "            <input id=\"log\" type=\"password\" tabindex=\"2\" form=\"data\" data-name=\"password\" name=\"password\"\n" +
                "                   placeholder=\"Password\">\n" +
                "        </div>\n" +
                "        <div id=\"log-in\">\n" +
                "            <button id=\"button\" tabindex=\"3\" type=\"submit\" src=\"images/backForm.jpg\">Log in</button>\n" +
                "            <a id=\"forgotpass\" href=\"index.html\">forgot your password?</a>\n" +
                "            <h5>Wrong login or password</h5>\n" +
                "        </div>\n" +
                "\n" +
                "    </form>\n" +
                "</div>\n" +
                "\n" +
                "</body>\n" +
                "</html>");
    }


}