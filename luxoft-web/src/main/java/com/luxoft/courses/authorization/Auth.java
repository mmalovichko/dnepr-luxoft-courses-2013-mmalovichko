package com.luxoft.courses.authorization;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

import static com.luxoft.courses.authorization.LoginServletLogic.getSessIdLogonStorage;

public class Auth extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession currentSession = req.getSession(false);
        if (currentSession == null) {
            resp.sendRedirect("http://localhost:8080/index.html");
        } else {
            currentSession.invalidate();
            resp.sendRedirect("http://localhost:8080/index.html");
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession(false) == null) {
            resp.sendRedirect("http://localhost:8080/index.html");
        } else {
            resp.setCharacterEncoding("utf-8");
            PrintWriter out = resp.getWriter();
            out.print("<!DOCTYPE HTML>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/authoriz.css\">\n" +
                    "    <title>Добро пожаловать</title>\n" +
                    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                    "</head>\n" +
                    "\n" +
                    "<body>\n" +
                    "<div id=\"hello\">\n" +
                    "    <h1>Hello," + getSessIdLogonStorage().get(req.getSession(false).getId()) + "</h1>\n" +
                    "\n" +
                    "</div>\n" +
                    "<div id=\"exit\">\n" +
                    "    <form id=\"data\" action=\"http://localhost:8080/user\" method=\"post\">\n" +
                    "        <button id=\"button\" type=\"submit\" name=\"out\"\n" +
                    "                src=\"images/backForm.jpg\">Log out\n" +
                    "        </button>\n" +
                    "\n" +
                    "    </form>\n" +
                    "\n" +
                    "</div>\n" +
                    "\n" +
                    "</body>\n" +
                    "\n" +
                    "\n" +
                    "</html>");
        }
    }
}
