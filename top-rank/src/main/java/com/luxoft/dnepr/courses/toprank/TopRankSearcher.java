package com.luxoft.dnepr.courses.toprank;


import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class TopRankSearcher {
    private static final double DEFAULT_DAMPING_FACTOR = 0.8;
    private static final int DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING = 10;


    public TopRankResults execute(List<String> urls) {
        //System.setProperty("java.net.useSystemProxies", "true"); // only for work machine
        TopRankExecutor topRankExecutor = new TopRankExecutor(DEFAULT_DAMPING_FACTOR, DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING);
        return topRankExecutor.execute(getURLContent(urls));
    }


    public TopRankResults execute(List<String> urls, double dampingFactor, int numberOfLoopsInRankComputing) {
        //System.setProperty("java.net.useSystemProxies", "true");
        TopRankExecutor topRankExecutor = new TopRankExecutor(dampingFactor, numberOfLoopsInRankComputing);
        return topRankExecutor.execute(getURLContent(urls));

    }

    private Map<String, String> getURLContent(List<String> urls) {
        Map<String, String> urlContent = new HashMap<>();
        for (String link : urls) {
            try {
                URL url = new URL(link);
                URLConnection connection = url.openConnection();
                try (Scanner scanner = new Scanner(connection.getInputStream())) {
                    StringBuffer content = new StringBuffer();
                    while (scanner.hasNextLine()) {
                        content.append(scanner.nextLine());
                    }
                    urlContent.put(link, content.toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return urlContent;
    }




}