package com.luxoft.dnepr.courses.toprank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TopRankExecutor {
    private double dampingFactor;
    private int numberOfLoopsInRankComputing;
    private double constant;


    public TopRankExecutor(double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
    }

    public TopRankResults execute(Map<String, String> urlContent) {

        TopRankResults results = new TopRankResults();
        results.setGraph(buildGraph(urlContent));
        results.setReverseGraph(buildReverseGraph(results.getGraph()));
        results.setIndex(buildIndex(urlContent));
        results.setRanks(buildRanks(urlContent, results));

        return results;
    }

    private Map<String, Double> buildRanks(Map<String, String> urlContent, TopRankResults results) {
        Map<String, Double> prevValueRank = buildPrevValueRank(urlContent);
        constant = (1.0 - dampingFactor) / urlContent.size();

        for (int i = 0; i < numberOfLoopsInRankComputing; i++) {
            for (Map.Entry<String, Double> entry : prevValueRank.entrySet()) {
                prevValueRank.put(entry.getKey(), summ(entry, results, prevValueRank));
            }
        }
        return prevValueRank;
    }


    private Map<String, Double> buildPrevValueRank(Map<String, String> urlContent) {
        double zeroLoopRank = 1.0 / urlContent.size();
        Map<String, Double> prevValueRank = new HashMap<>(urlContent.size());
        for (String key : urlContent.keySet()) {
            prevValueRank.put(key, zeroLoopRank);
        }
        return prevValueRank;
    }


    private Map<String, List<String>> buildIndex(Map<String, String> urlContent) {
        Map<String, List<String>> index = new HashMap<>();
        for (String key : urlContent.keySet()) {
            String[] words = urlContent.get(key).split("[\t\n\\s]");
            for (String word : words) {
                if (!index.containsKey(word)) {
                    index.put(word, new ArrayList<String>());
                }
                index.get(word).add(key);

            }
        }
        return index;
    }


    private Map<String, List<String>> buildGraph(Map<String, String> urlContent) {
        Map<String, List<String>> resultGraph = new HashMap<>();
        for (String key : urlContent.keySet()) {
            resultGraph.put(key, new ArrayList<String>());
            for (Map.Entry<String, String> entry : urlContent.entrySet()) {
                if (checkLinkonPage(entry.getValue(), key)) {
                    resultGraph.get(key).add(entry.getKey());
                }

            }

        }

        return resultGraph;
    }

    private boolean checkLinkonPage(String value, String key) {
        Pattern p = Pattern.compile("<a href=\"" + key + "\">");
        Matcher m = p.matcher(value);
        return m.find();

    }


    private double summ(Map.Entry<String, Double> entry, TopRankResults res, Map<String, Double> prevVal) {
        double mainResult = 0;
        double result = 0;
        List<String> list = res.getGraph().get(entry.getKey());
        for (int i = 0; i < list.size(); i++) {
            result = dampingFactor * prevVal.get(list.get(i)) / res.getReverseGraph().get(list.get(i)).size();
            mainResult += result;
        }
        mainResult += constant;

        return mainResult;
    }


    private Map<String, List<String>> buildReverseGraph(Map<String, List<String>> graph) {
        Map<String, List<String>> result = new HashMap<>();
        for (String key : graph.keySet()) {
            result.put(key, new ArrayList<String>());
            for (Map.Entry<String, List<String>> entry : graph.entrySet()) {
                List<String> list = entry.getValue();
                for (String s : list) {
                    if (key.equals(s)) {
                        result.get(key).add(entry.getKey());

                    }
                }

            }

        }

        return result;
    }



}