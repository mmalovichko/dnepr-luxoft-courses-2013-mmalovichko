<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false" %>
<!--<%@ include file="../../resources/styles/mainStyle.css" %>-->


<html>
<head>
    <title>Добро пожаловать</title>
    <link rel="stylesheet" media="screen" type="text/css" href="../../resources/styles/mainStyle.css"/>
</head>
<body>
<div id="hello">
    <h1>Hello, <%= request.getParameter("login")%>!</h1>

</div>
<div id="exit">
    <a href="logout">
        <button id="button" src="../../resources/images/backBody.jpg">Log out</button>
    </a>

</div>
<%= request.getSession(false).getAttribute(request.getParameter("login"))%>

<div id="admin">
    <a href="admin/sessionData">
        <button id="button" src="../../resources/images/backBody.jpg">go to admin!</button>
    </a>

</div>


</body>
</html>