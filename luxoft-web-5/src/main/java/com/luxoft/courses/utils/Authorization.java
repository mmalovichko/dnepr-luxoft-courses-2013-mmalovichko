package com.luxoft.courses.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Authorization extends UserAttribute {


    public static boolean isLoginPasswordValid(HttpServletRequest request) {

        String password = request.getParameter("password");
        String login = request.getParameter("login");

        System.out.println("login "+login);
        System.out.println("pawwsord "+password);

        if (login == null || password == null) {
            return false;

        } else
            return (getUsers().containsKey(login)
                    && getUserPassword(request).equals(password))
                    ? true : false;
    }



    public static void createSession(HttpServletRequest request, HttpServletResponse response) {

        String sessionID = request.getSession().getId();
        String login = request.getParameter("login");

        request.getSession().setAttribute(login, getUserRole(request));
        getSessIdLogonStorage().put(sessionID, login);

        System.out.println("session: " + getSessIdLogonStorage().size());

    }


}
