package com.luxoft.courses.utils;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserAttribute implements AppConstant {

    private static Map<String, ArrayList<String>> users = new ConcurrentHashMap<>();

    public static Map<String, String> getSessIdLogonStorage() {
        return sessIdLogonStorage;
    }

    public static void setSessIdLogonStorage(Map<String, String> sessIdLogonStorage) {
        UserAttribute.sessIdLogonStorage = sessIdLogonStorage;
    }

    private static Map<String, String> sessIdLogonStorage = new ConcurrentHashMap<>();

    public static Map<String, ArrayList<String>> getUsers() {
        return users;
    }

    public static void createUserMap(String json) {
        Gson gson = new Gson();
        users = gson.fromJson(json, new TypeToken<Map<String, ArrayList<String>>>() {
        }.getType());
        System.out.println("size of users: "+users.size());

    }



    public static String getUserRole(HttpServletRequest request) {

        String login = request.getParameter("login");
        //return request.getSession(false).getAttribute(login);
        System.out.println(request.getSession(false).getAttribute(login));
        return users.get(login).get(GET_USER_ROLE);

    }

    public static String getUserPassword(HttpServletRequest request) {

        String login = request.getParameter("login");
        return users.get(login).get(GET_USER_PASSWORD);

    }







}
