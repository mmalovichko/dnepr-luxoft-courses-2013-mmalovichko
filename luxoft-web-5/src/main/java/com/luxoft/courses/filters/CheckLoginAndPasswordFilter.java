package com.luxoft.courses.filters;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.luxoft.courses.utils.Authorization.createSession;
import static com.luxoft.courses.utils.Authorization.isLoginPasswordValid;

public class CheckLoginAndPasswordFilter implements Filter{


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse)servletResponse;
        HttpServletRequest request = (HttpServletRequest)servletRequest;

        if (isLoginPasswordValid(request)){
            createSession(request, response);
            System.out.println("create session");
            filterChain.doFilter(servletRequest, servletResponse);
        } else{
            System.out.println("invalid password");
            response.sendRedirect("index.jsp");
        }
    }




    @Override
    public void destroy() {

    }
}
