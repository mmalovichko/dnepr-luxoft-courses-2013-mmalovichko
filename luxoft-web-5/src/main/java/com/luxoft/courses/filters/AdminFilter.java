package com.luxoft.courses.filters;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.luxoft.courses.utils.UserAttribute.getUserRole;

public class AdminFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        System.out.println("admin filter");

        if (getUserRole(request).equals("admin")) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else if (getUserRole(request).equals("user") && request.getSession(false) != null) {
            response.sendRedirect("user.jsp");
        } else {
            response.sendRedirect("/index.jsp");
        }

    }

    @Override
    public void destroy() {

    }
}
