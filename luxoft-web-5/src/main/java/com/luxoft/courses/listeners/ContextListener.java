package com.luxoft.courses.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static com.luxoft.courses.utils.Authorization.createUserMap;


public class ContextListener implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        String json = servletContextEvent.getServletContext().getInitParameter("users");
        System.out.println("contextInitialized");
        createUserMap(json);

    }



    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
