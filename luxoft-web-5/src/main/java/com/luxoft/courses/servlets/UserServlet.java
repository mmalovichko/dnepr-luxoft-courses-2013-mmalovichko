package com.luxoft.courses.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.luxoft.courses.utils.UserAttribute.getSessIdLogonStorage;


public class UserServlet extends HttpServlet {



//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        System.out.println("user servlet get");
//
//    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("user servlet post"+"user login: "+req.getParameter("login"));
        //resp.sendRedirect("user.jsp");

        String nextJSP = "/user.jsp";
        req.getSession(false).setAttribute(req.getParameter("login").toString(), getSessIdLogonStorage().get(req.getSession().getId()));
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req,resp);

    }
}
